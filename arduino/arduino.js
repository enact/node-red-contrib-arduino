module.exports = function(RED) {
    "use strict";
    var ArduinoEngine = require("./arduino-engine");

    function ArduinoNode(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        var engine = new ArduinoEngine();

        node.serial = RED.nodes.getNode(config.serial);

        // Received message, trigger deploy
        //TODO: if we trigger build by message and the previous build didn't finish, what to do?
        node.on('input', function(msg) {
            var serialNodeFound = false;
            node.status({fill:"yellow",shape:"dot",text:"arduino.status.deploying"});

            // Force close the serial port if a serial node opened it
            RED.nodes.eachNode(function(nodeRef) {
                if (!serialNodeFound && nodeRef.type.startsWith("serial ") && nodeRef.serial === config.serial) {
                    if (!RED.nodes.getNode(nodeRef.id).port.serial.isOpen) { // If port is not open, no need to close and open it again
                        return;
                    }
                    serialNodeFound = true;
                    node.serialNodeReference = RED.nodes.getNode(nodeRef.id);
                    node.serialNodeReference.port._closing = true;
                    node.log("Closing " + node.serial.serialport + " serial port…");
                    node.serialNodeReference.port.serial.close(function(err) {
                        node.serialNodeReference.port._closing = false;
                        if (err) {
                            node.error(err);
                        } else {
                            node.log("Serial port " + node.serial.serialport + " closed.");
                        }
                        engine.deploy(node, config, msg.payload);
                    });
                }
            });
            if (!serialNodeFound) {
                engine.deploy(node, config, msg.payload);
            }
        });

        engine.on('deployed', function() {
            // reopening previously closed port
            if (node.serialNodeReference) {
                node.log("Reopening " + node.serial.serialport + " serial port…");
                node.serialNodeReference.port.serial.open(function(err) {
                    if (err) {
                        node.error(err);
                    }
                });
            }
            node.status({fill:"green",shape:"dot",text:"arduino.status.success"});
            var pload = {status: 'success', cfg: config};
            var msg = { payload: pload };
            node.send(msg);
        });

        engine.on('error', function(e) {
            node.status({fill:"red",shape:"ring",text:"node-red:common.status.error"});
            var pload = {status: 'error', cfg: config};
            var msg = { payload: pload };
            node.send(msg);
            node.error(e);
        });

        // cleanup on flow stop
        node.on("close", function (done) {
            engine.close(node);
            done();
        });
    }
    RED.nodes.registerType("arduino", ArduinoNode);
}
