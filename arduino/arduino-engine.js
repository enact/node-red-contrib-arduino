//This function call ThingML for compilation
var spawn = require('child_process').spawn;
var events = require('events');
var os = require('os');

module.exports = class ArduinoEngine {
    constructor() {
        this.emitter = new events.EventEmitter();
    }

    //This function calls Arduino ID to compile Arduino sketches
    // and to deploy them
    deploy(node, config, msg) {
        //We Should install libaries first
        var path_to_ardui = "";
        if (os.platform() === "darwin") {
            path_to_ardui = '/Applications/Arduino.app/Contents/MacOS/Arduino';
        } else {
            path_to_ardui = 'arduino';
        }

        var tab;
        try {
            tab = JSON.parse(config.libraries);
            this.installLibraries(node, config, msg, path_to_ardui, tab, 0);
        } catch (e) {
            node.error(e + ": " + config.libraries);
        }
    }

    installLibraries(node, config, msg, path_to_ardui, tab, k) {
        var engine = this;
        if (config.libraries !== undefined && config.libraries !== "") {
            node.log(JSON.stringify(tab));
            node.log(path_to_ardui);
            if (k < tab.length) {
                node.log("installing library");
                var install = spawn(path_to_ardui, [
                    '--install-library', tab[k]
                ]);

                install.stdout.setEncoding('utf8');
                install.stdout.on('data', (data) => {
                    data.trim().split('\n').forEach(line => {
                        node.log(line);
                    });
                });

                install.stderr.setEncoding('utf8');
                install.stderr.on('data', (data) => {
                    data.trim().split('\n').forEach(line => {
                        node.warn(line);
                    });
                });

                install.on('error', (err) => {
                    node.error('Something went wrong with the installation of libraries!' + err);
                });

                install.on('exit', (code) => {
                    node.log('Installation of ' + tab[k] + ' Completed!');
                    engine.installLibraries(node, config, msg, path_to_ardui, tab, k + 1);
                });
            } else {
                this.upload(node, config, msg, path_to_ardui);
            }

        } else {
            this.upload(node, config, msg, path_to_ardui);
        }

    }

    upload(node, config, msg, path_to_ardui) {
        var engine = this;
        var hasError = false;
        var board = 'arduino:avr:' + config.ardtype;
        if (config.cpu !== "") {
            board += ':cpu=' + config.cpu;
        }
        node.log('Board' + board);
        node.process = spawn(path_to_ardui, [
            '--board', board,
            '--port', node.serial.serialport,
            '--upload', msg.output + '/' + msg.name + '/' + msg.name + '.ino',
        ]);

        node.process.stdout.setEncoding('utf8');
        node.process.stdout.on('data', (data) => {
            data.trim().split('\n').forEach(line => {
                if (line.toLowerCase().includes('warning')) {
                    node.warn(line);
                } else if (line.toLowerCase().includes('error')) {
                    hasError = true;
                    node.error(line);
                } else {
                    node.log(line);
                }
            });
        });
        //apparently Arduino logs on standard error
        node.process.stderr.setEncoding('utf-8');
        node.process.stderr.on('data', (data) => {
            data.trim().split('\n').forEach(line => {
                if (line.toLowerCase().includes('warning')) {
                    node.warn(line);
                } else if (line.toLowerCase().includes('error')) {
                    hasError = true;
                    node.error(line);
                } else {
                    node.log(line);
                }
            });
        });

        node.process.on('error', (err) => {
            hasError = true;
            node.error('Something went wrong with the upload!' + err);
        });

        node.process.on('exit', (code) => {
            if (code !== 0) {
                engine.emitter.emit('error', 'Exit code: ' + code);
            } else if (hasError) {
                engine.emitter.emit('error', 'Cannot complete because of errors!');
            } else {
                node.log('[INFO] Upload completed!: ' + config.name);
                engine.emitter.emit('deployed');
            }
            delete node.process;
        });
    }

    close(node) {
        if (node.process) {
            node.process.kill();
        }
        delete node.process;
        //TODO: deploy empty sketch
        this.emitter.removeAllListeners();
    };

    on(e, f) {
        this.emitter.on(e, f);
    }
}
